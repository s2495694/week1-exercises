package nl.utwente.di.bookQuote;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Calculator", value = "/Calculator")
public class ServletDegreeCalculator extends HttpServlet {
    private Calculator calculator;

    public void init() throws ServletException {
        calculator = new Calculator();
    }

    @Override
    public void doGet(
        HttpServletRequest servletRequest,
        HttpServletResponse servletResponse
    ) throws ServletException, IOException {

        servletResponse.setContentType("text/html");
        PrintWriter out = servletResponse.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Degree Calculator";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Changed Degrees: " +
                servletRequest.getParameter("degrees") + "\n" +
                "  <P>Degrees in Farenheit: " +
                Double.toString(calculator.calculateDegrees(servletRequest.getParameter("degrees"))) +
                "</BODY></HTML>");
    }
}

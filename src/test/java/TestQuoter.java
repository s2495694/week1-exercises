import nl.utwente.di.bookQuote.Quoter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TestQuoter {

    @Test
    public void testGetBookPrice(){
        Quoter quoter = new Quoter();

        double price = quoter.getBookPrice("1");
        Assertions.assertEquals(10, price, 0, "isbn 1 =");

        price = quoter.getBookPrice("2");
        Assertions.assertEquals(45, price, 0, "isbn 2 =");

        price = quoter.getBookPrice("3");
        Assertions.assertEquals(20, price, 0, "isbn 3 =");

        price = quoter.getBookPrice("4");
        Assertions.assertEquals(35, price, 0, "isbn 4 =");
    }
}
